<?php
date_default_timezone_set('Asia/Karachi');
error_reporting(1);

class Account
{
  
    private $response;
    public function check_input($data = null, $fields_required = null)

    {
        $status = FALSE;
        $i = 0;
        $incoming_fields = array();
        $fields = '';
        if ($data == NULL || $data == '')
        {
            foreach ($fields_required as $required)
            {
                $fields.=' ' . $required . ',';
            }
            $fields = rtrim($fields, ",");
            $this->response = array(
                "info" => array(
                                            'status' => '400',

                    'message' => 'Need' . $fields . ' to be supplied with service call')    );
            return false;
        }
        else
        {
            foreach ($data as $key => $value) {
                if ($value == '' && in_array($key, $fields_required))
                {
                    $fields.=' ' . $key . ',';
                    $status = TRUE;
                }
                $incoming_fields[$i] = $key;
                $i++;
            }
            foreach ($fields_required as $to_check)
            {
                if (!in_array($to_check, $incoming_fields))

                {
                    $fields.=' ' . $to_check . ',';
                    $status = TRUE;
                }
            }
            if ($status == TRUE)
            {
                $this->response = array(
                    
                        'status' => 'failure',
                        'code' => '10',
                        'message' => 'Need' . $fields . ' to be supplied with service call'
                    );
                return FALSE;
            }
        }
        return TRUE;
    }
   public function get_states($data){
        
        $conn = mysqli_connect('localhost', 'root', 'T@ke!LiVe' , 'php_api') OR die(mysqli_error($conn));
        $res = mysqli_query($conn,"SELECT `StateID`,`StateName` FROM `dbo.State`");
        if(mysqli_num_rows($res)>0){
            $results = [];
            while ( $row = mysqli_fetch_assoc($res) ) {

                 $obj['Id'] = (int)$row['StateID'];
                 $obj['State'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['StateName']);
                 $results[] = $obj;
            }
            return  array(
                        'body' => array(
                            'status' => 'success',
                            'state' => $results
                        )
                    );
        }else{
            return  array(
                        'body' => array(
                            'status' => 'failure',
                            'state' => []
                        )
                    );
        }
   }

   public function get_categories($data){
        
        $conn = mysqli_connect('localhost', 'root', 'T@ke!LiVe' , 'php_api') OR die(mysqli_error($conn));
        $res = mysqli_query($conn,"SELECT `CoverageID`, `CoverageName` FROM `dbo.Coverage` ");

        if(mysqli_num_rows($res)>0){
            $results = [];
            while ( $row = mysqli_fetch_assoc($res) ) {
                 $obj['Id'] = (int)$row['CoverageID'];
                 $obj['CoverageName'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['CoverageName']);
                 $results[] = $obj;
            }

            return array(
                        'body' => array(
                            'status' => 'success',
                            'Coverage' => $results
                        )
                    );
        }else{
            return array(
                        'body' => array(
                            'status' => 'failure',
                            'Coverage' => []
                        )
                    );
        }
   }
 public function get_all_companies(){
        $conn = mysqli_connect('localhost', 'root', 'T@ke!LiVe' , 'php_api') OR die(mysqli_error($conn));
        $res = mysqli_query($conn,"SELECT `CompanyID`, `CompanyName` FROM `dbo.Company` WHERE `FiscalYear` = '2018' ORDER BY CompanyName ASC");
        if(mysqli_num_rows($res)>0){
            $results = [];
            while ( $row = mysqli_fetch_assoc($res) ) {
                 $obj['CompanyID'] = $row['CompanyID'];
                 $obj['CompanyName'] = preg_replace('/[^A-Za-z0-9\-]/', '', $row['CompanyName']);
                 $results[] = $obj;
            }
		return array(
                        'body' => array(
                            'status' => 'success',
                            'Companies' => $results
                        )
                    );
        }else{
            return array(
                        'body' => array(
                            'status' => 'success',
                            'Companies' => []
                        )
                    );
        }
    }
    
   public function get_companies($data){

       $state = $data['stateID'];
       $categoryId = $data['categoryId'];
       $year = date("Y");
        $conn = mysqli_connect('localhost', 'root', 'T@ke!LiVe' , 'php_api') OR die(mysqli_error($conn));

        $str = "select `dbo.Company`.`CompanyID`,`dbo.Company`.`CompanyName` from `dbo.CompanyCoverage` ";
        $str .= "left join `dbo.Company` on `dbo.Company`.`CompanyID`=`dbo.CompanyCoverage`.`CompanyID` left join `dbo.CompanyOperatingState` on ";
        $str .= "`dbo.CompanyOperatingState`.`CompanyID`=`dbo.CompanyCoverage`.`CompanyID` where ";
        $str .= "`dbo.CompanyOperatingState`.`StateID`= ".$state." and `dbo.CompanyCoverage`.`CoverageID`= ".$categoryId." and `dbo.CompanyCoverage`.`CoverageID`= ".$categoryId." and `dbo.Company`.`FiscalYear`=".$year;

        $res = mysqli_query($conn,"select `dbo.Company`.`CompanyID`,`dbo.Company`.`CompanyName` from `dbo.CompanyCoverage` left join `dbo.Company` on `dbo.Company`.`CompanyID`=`dbo.CompanyCoverage`.`CompanyID` left join `dbo.CompanyOperatingState` on `dbo.CompanyOperatingState`.`CompanyID`=`dbo.CompanyCoverage`.`CompanyID` where `dbo.CompanyOperatingState`.`StateID`= ".$state." and `dbo.CompanyCoverage`.`CoverageID`= ".$categoryId." and `dbo.CompanyCoverage`.`CoverageID`= ".$categoryId." and `dbo.Company`.`FiscalYear`=".$year);

        $query = "SELECT `CoverageID`, `CoverageName` , `CoverageDescription` FROM `dbo.Coverage` WHERE `CoverageID` = ".$categoryId;
        $result = mysqli_query($conn,$query);
        $response = array(
                'result' => array(
                    'CoverageID' => '',
                    'CoverageName' =>  '',
                    'CoverageDescription' =>  '',
                    'companies' => []
                )
            );

        if(mysqli_num_rows($result)>0){
            $row = mysqli_fetch_assoc($result) ;
            $response = array(
                'result' => array(
                    'CoverageID' => $row['CoverageID'],
                    'CoverageName' =>  $row['CoverageName'],
                    'CoverageDescription' =>  $row['CoverageDescription'],
                    'companies' => []
                )
            );
        }


        if(mysqli_num_rows($res)>0){
            $results = [];

            while ( $row = mysqli_fetch_assoc($res) ) {
                 $obj['CompanyID'] = $row['CompanyID'];
                 $obj['CompanyName'] = $row['CompanyName'];
                 $results[] = $obj;
            }
            $response['result']['companies'] = $results;

            return $response;
        }else{
            return [];
        }
   }
    
}
?>



