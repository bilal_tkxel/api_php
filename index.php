<?php
include_once('includes/account.php');

header('Content-type: application/json');
// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
// header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
$json_d = file_get_contents('php://input');


$new_data = $json_d;
$json = json_decode($json_d, true);
$method = $_SERVER['REQUEST_METHOD'];
$req_uri = $_SERVER['REQUEST_URI'];
$req_uri = explode('/', $req_uri);
$obj_account = new Account();

switch ($method) {
    case 'GET':
        if ($req_uri[2] == "api"){

                if ($req_uri[3] == "get_states") {
                        // $json_data = json_decode($json_d, true);
                        $response = $obj_account->get_states(); 
                        echo json_encode($response);die();
                        exit();                 
                } 
                else  if ($req_uri[3] == "get_categories") {
                        $response = $obj_account->get_categories(); 
                        echo json_encode($response);die();
                        exit();  
                }
		else  if ($req_uri[3] == "get_all_companies") {
                    $obj_account = new Account();

                        $response = $obj_account->get_all_companies(); 
                        echo json_encode($response);
                        exit();  
                }
                else {
                    $response = array(
                        'body' => array(
                            'status' => 'empty',
                            'message' => 'no function yet registered for method GET'
                        )
                    );
                }
               
        }
       
                exit();
    case 'POST':
            if ($req_uri[2] == "api"){
            if ($req_uri[3] == "get_companies") {
                    $obj_account = new Account();
                        $response = $obj_account->get_companies($json); 
                        echo json_encode($response);
                        exit();  
                }
                else {
                    $response = array(
                        'body' => array(
                            'status' => 'empty',
                            'message' => 'no function yet registered for method GET'
                        )
                    );
                }
               
        }
        
    
      break;
    case 'PUT':
        $response = array(
            'register' => array(
                'status' => 'empty',
                'message' => 'no function yet registerd for method PUT'
            )
        );
        echo json_encode($response);
        exit();
        break;

    case 'DELETE':
        $response = array(
            'register' => array(
                'status' => 'empty',
                'message' => 'no function yet registerd for method DELETE'
            )
        );
        echo json_encode($response);
        exit();
        break;
}

// basic function to clean incoming values
function clean_input($data) {
    if ($data == NULL) {
        return NULL;
    }
    foreach ($data as $key => $value) {
        $data[$key] = mysql_escape_string($value);
    }
    return $data;
}

function makeCDATA($text) {
    return "<![CDATA[" . $text . "]]>";
}

?>
